
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "fmixtest.h"
#include <string.h>
#include <R.h>
#include <Rinternals.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
int
is2by2(SEXP arg)
{
	/* Check if we have a 2-by-2 matrix */
	return isMatrix(arg) && nrows(arg) == 2 && ncols(arg) == 2;
}

static
int
is4byN(SEXP arg)
{
	/* Check if we have a 4 element vector or a 4-by-N matrix */
	return isVector(arg) && length(arg) == 4 ||
		isMatrix(arg) && nrows(arg) == 4;
}

static
int
checkAlternative(int *tail, const char *alt)
{
	/* Compare string */
	switch (alt[0])
	{
		case 'g':
			if (alt[1] == '\0' || strcmp(alt, "greater") == 0)
			{
				*tail = +1;
				return 1;
			}
			break;
		case 'l':
			if (alt[1] == '\0' || strcmp(alt, "less") == 0)
			{
				*tail = -1;
				return 1;
			}
			break;
		case 't':
			if (alt[1] == '\0' || strcmp(alt, "two.sided") == 0)
			{
				*tail = 0;
				return 1;
			}
			break;
	}

	/* No match */
	return 0;
}

static
int
isCount(unsigned long *count, double value)
{
	/* Cast & check */
	*count = (unsigned long)value;
	return (double)*count == value;
}

SEXP
dmfisher_fmixtest_R(SEXP args)
{
	SEXP x_R, mcomm_R, pcomm_R, alt_R, p_R;
	int tail, tables, j, i, scratch_size;
	const double *xvals, *mcomm;
	double pcomm, *pvals, *scratch;
	unsigned long sumcounts, counts[4];

	/* Check input count */
	if (length(args) - 1 != 4)
		error("%d arguments passed to '%s' which requires %d", length(args) - 1, "dmfisher.fmixtest", 4);

	/* Get inputs */
	x_R = CADR(args);
	mcomm_R = CADDR(args);
	pcomm_R = CADDDR(args);
	alt_R = CAD4R(args);

	/* Check inputs */
	if (!(isReal(x_R) && isNumeric(x_R) && (is2by2(x_R) || is4byN(x_R))))
		error("'%s' must be a real numeric 2-by-2 or 4-by-n matrix", "x");
	if (!(isReal(mcomm_R) && isNumeric(mcomm_R) && length(mcomm_R) == 2))
		error("'%s' must be a real numeric 2-vector", "mcomm");
	if (!(isReal(pcomm_R) && isNumeric(pcomm_R) && length(pcomm_R) == 1))
		error("'%s' must be a real numeric scalar", "pcomm");
	if (!(isString(alt_R) && length(alt_R) == 1 && checkAlternative(&tail, CHAR(STRING_ELT(alt_R, 0)))))
		error("'%s' must be 'greater', 'less', or 'two.sided'", "alternative");

	/* Create output */
	tables = length(x_R) / countof(counts);
	p_R = PROTECT(allocVector(REALSXP, tables));

	/* Get data */
	xvals = REAL(x_R);
	mcomm = REAL(mcomm_R);
	pcomm = *REAL(pcomm_R);
	pvals = REAL(p_R);

	/* Set up scratch space */
	scratch = NULL;
	scratch_size = 0;

	/* Compute */
	for (j = 0; j < tables; ++j)
	{
		/* Check values */
		for (i = 0; i < countof(counts); ++i)
			if (!isCount(&counts[i], xvals[i + j * countof(counts)]))
				error("'%s' must contain non-negative integers", "x");

		/* Make sure we have the scratch space */
		sumcounts = counts[0] + counts[1] + counts[2] + counts[3];
		if (scratch_size < sumcounts + 2)
		{
			scratch = (double *)R_alloc(sizeof(*scratch), sumcounts + 2);
			scratch_size = sumcounts + 2;
		}

		/* Perform the test */
		pvals[j] = fmixtest(scratch, counts, mcomm, pcomm, tail);
	}

	UNPROTECT(1);
	return p_R;
}
