
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "fishertest.h"
#include <string.h>
#include <R.h>
#include <Rinternals.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
int
is2by2(SEXP arg)
{
	/* Check if we have a 2-by-2 matrix */
	return isMatrix(arg) && nrows(arg) == 2 && ncols(arg) == 2;
}

static
int
is4byN(SEXP arg)
{
	/* Check if we have a 4 element vector or a 4-by-N matrix */
	return isVector(arg) && length(arg) == 4 ||
		isMatrix(arg) && nrows(arg) == 4;
}

static
int
checkAlternative(int *tail, const char *alt)
{
	/* Compare string */
	switch (alt[0])
	{
		case 'g':
			if (alt[1] == '\0' || strcmp(alt, "greater") == 0)
			{
				*tail = +1;
				return 1;
			}
			break;
		case 'l':
			if (alt[1] == '\0' || strcmp(alt, "less") == 0)
			{
				*tail = -1;
				return 1;
			}
			break;
		case 't':
			if (alt[1] == '\0' || strcmp(alt, "two.sided") == 0)
			{
				*tail = 0;
				return 1;
			}
			break;
	}

	/* No match */
	return 0;
}

static
int
isCount(unsigned long *count, double value)
{
	/* Cast & check */
	*count = (unsigned long)value;
	return (double)*count == value;
}

SEXP
dmfisher_fishertest_R(SEXP args)
{
	SEXP x_R, alt_R, log_R, p_R;
	int tail, tables, use_log, j, i;
	const double *xvals;
	double *pvals;
	unsigned long counts[4];

	/* Check input count */
	if (length(args) - 1 != 3)
		error("%d arguments passed to '%s' which requires %d", length(args) - 1, "dmfisher.fishertest", 3);

	/* Get inputs */
	x_R = CADR(args);
	alt_R = CADDR(args);
	log_R = CADDDR(args);

	/* Check inputs */
	if (!(isReal(x_R) && isNumeric(x_R) && (is2by2(x_R) || is4byN(x_R))))
		error("'%s' must be a real numeric 2-by-2 or 4-by-n matrix", "x");
	if (!(isString(alt_R) && length(alt_R) == 1 && checkAlternative(&tail, CHAR(STRING_ELT(alt_R, 0)))))
		error("'%s' must be 'greater', 'less', or 'two.sided'", "alternative");
	if (!(isLogical(log_R) && length(log_R) == 1))
		error("'%s' must be a logical", "log.p");

	/* Create output */
	tables = length(x_R) / countof(counts);
	p_R = PROTECT(allocVector(REALSXP, tables));

	/* Get data */
	xvals = REAL(x_R);
	pvals = REAL(p_R);
	use_log = *LOGICAL(log_R);

	/* Compute */
	for (j = 0; j < tables; ++j)
	{
		/* Check values */
		for (i = 0; i < countof(counts); ++i)
			if (!isCount(&counts[i], xvals[i + j * countof(counts)]))
				error("'%s' must contain non-negative integers", "x");

		/* Perform the test */
		if (!use_log)
			pvals[j] = fishertest(counts, tail);
		else
			pvals[j] = fishertest_log(counts, tail);
	}

	UNPROTECT(1);
	return p_R;
}
