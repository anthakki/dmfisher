
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "gaussquad.h"
#include <R.h>
#include <Rinternals.h>

SEXP
dmfisher_gaussquad_R(SEXP args)
{
	SEXP n_R, nodes_R, weights_R, output_R;
	int order;

	/* Check input count */
	if (length(args) - 1 != 1)
		error("%d arguments passed to '%s' which requires %d", length(args) - 1, "dmfisher.gaussquad", 1);

	/* Get inputs */
	n_R = CADR(args);

	/* Check input types */
	if (!(isReal(n_R) && isNumeric(n_R) && length(n_R) == 1))
		error("'%s' must be a real numeric scalar", "n");

	/* Get order */
	order = (int)*REAL(n_R);
	if (!(order == *REAL(n_R) && order >= 0))
		error("'%s' must represent a non-negative integer", "n");

	/* Create output arrays */
	nodes_R = PROTECT(allocVector(REALSXP, order));
	weights_R = PROTECT(allocVector(REALSXP, order));

	/* Populate */
	gaussquad(REAL(nodes_R), REAL(weights_R), (size_t)order);

	/* Create output tuple */
	output_R = PROTECT(allocList(2));
	SETCAR(output_R, nodes_R);
	SET_TAG(output_R, install("nodes"));
	SETCAR(CDR(output_R), weights_R);
	SET_TAG(CDR(output_R), install("weights"));

	UNPROTECT(3);
	return output_R;
}
