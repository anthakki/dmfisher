
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "bhfdr.h"
#include "sortinds.h"
#include <string.h>
#include <R.h>
#include <Rinternals.h>

SEXP
dmfisher_bhfdr_R(SEXP args)
{
	SEXP p_R, method_R, mut_p_R;
	void (*method)(double *, const size_t *, size_t);
	int n;
	double *pvals;
	size_t *inds;

	/* Check input count */
	if (length(args) - 1 != 2)
		error("%d arguments passed to '%s' which requires %d", length(args) - 1, "dmfisher.bhfdr", 2);

	/* Get inputs */
	p_R = CADR(args);
	method_R = CADDR(args);

	/* Check input types */
	if (!(isReal(p_R) && isNumeric(p_R) && isVector(p_R)))
		error("'%s' must be a real numeric vector", "p");
	if (!(isString(method_R) && length(method_R) == 1))
		error("'%s' must be a string", "method");

	/* Parse method string */
	if (strcmp(CHAR(STRING_ELT(method_R, 0)), "BH") == 0)
		method = &bhfdr;
	else if (strcmp(CHAR(STRING_ELT(method_R, 0)), "BY") == 0)
		method = &byfdr;
	else
		error("'%s' must be 'BH' or 'BY'", "method");

	/* Create output */
	n = length(p_R);
	mut_p_R = PROTECT(allocVector(REALSXP, n));

	/* Get working arrays */
	pvals = REAL(mut_p_R);
	inds = (size_t *)R_alloc(sizeof(*inds), n);

	/* Adjust p-values */
	memcpy(pvals, REAL(p_R), (size_t)n * sizeof(*pvals));
	(*method)(pvals, inds, sortinds(inds, pvals, (size_t)n));

	UNPROTECT(1);
	return mut_p_R;
}
