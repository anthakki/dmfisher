
\name{dmfisher.bhfdr}
\alias{dmfisher.bhfdr}

\title{ Adjust p-values using the Benjamini-Hochberg procedure }
\description{
	Adjusts p-values for false discovery rate using the Benjamini-Hochberg
	procedure. This allows controlling the false discovery rate when performing
	multiple hypothesis testing.
}

\usage{ padj <- dmfisher.bhfdr(p) }
\arguments{
  \item{p}{ A vector of p-values }
}
\details{}
\value{ Returns the corresponding adjusted p-values. }

\references{}
\author{ \packageAuthor{dmfisher} }
\note{}

\seealso{
	See also \code{\link{p.adjust}}
}
\examples{}
\keyword{ htest }
