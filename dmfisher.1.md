% DMFISHER(1) Version 0.9 | dmfisher documentation

NAME
====

**dmfisher** -- Differential methylation using Fisher's exact test

SYNOPSIS
========

| **dmfisher** \[**-BHILQS**\] \[**-pqc** _parm_\] \[**-t** _tail_\] \[**-a** _adjust_\] \[**-fds**\] _file_ \[...\]

DESCRIPTION
===========

The tool reads a pair of methylation counts from the specified input files, performs a Fisher's exact test between the two sets of counts, and writes the p-values of the hypothesis test to standard output.

The input is assumed to be a series of four integers representing the counts of unmethylated/methylated reads in the left and right samples, as specified by the input format specifiers. More fields are read if specified by **-p**, **-q**, or **-c**.

Options
-------

-B, -H, -I, -L, -Q

:   Specifies that the input is binary 8-, 16-, 32-, 32-, or 64-bit, respectively, unsigned integers of the native endianess.

-S

:   Specifies that the input is ASCII, separated by any whitespace. In particular, the records do not need to be on different lines.

-p _parm_, -q _parm_

:   Sets the purity of the left and right samples (default: 1), respectively. These options are used to trigger an adjustment that corrects for the sample impurities. If **-p** or **-q** are specified, **-c** must also be provided. The input _parm_ can be a constant fraction in [0,1] or `B`, `H`, `I`, `L`, `Q`, `S`, `f`, or `d` to specify that the value is to be read from the input stream.

-c _parm_

:   Sets the average methylation of the control sample. Again, the argument can be either a constant or a input type specifier, as for **-p** and **-q**.

-t _tail_

:   Specifies the tail to be used in the alternative hypothesis. Can be `both` (default), `left`, or `right` to test for differential methylation, hypermethylation (hypo-), or hypomethylation (hyper-) in the left (right) sample.

-a _adjust_

:   Specifies an adjustment on the p-values after the hypothesis test. Note that an adjustment might disable the stream processing ability. Currently, the supported methods: `fdr` adjusts for false discovery rate using the Benjamini-Hochberg procedure.

-f, -d

:   Specifies that the output is binary single or double precision, respectively, native floating point numbers (typically, but not necessarily IEEE floats) in native endianess. 

-s

:   Specifies that the output is ASCII, separated by whitespace.

BUGS
====

Input validation is minimal.
An output file cannot be specified.
The mixture adjusted test has quadratic time complexity.

SEE ALSO
========

MATLAB and R interfaces are available.

AUTHOR
======

Antti Hakkinen.
