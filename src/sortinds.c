
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "sortinds.h"
#include <assert.h>
#include <math.h> /* isnan() */

/* TODO: sort NaNs by payload */

#ifndef isnan
#	define isnan(x) ((x) != (x))
#endif

size_t
sortinds(size_t *inds, const double *vals, size_t count)
{
	size_t n, i, p, j, t;

	assert((inds != NULL || count < 1) && (vals != NULL || count < 1));

	/* Populate indices and put NaNs at the end */
	p = 0;
	n = count;
	for (i = 0; i < count; ++i)
		if (!isnan(vals[i]))
			inds[p++] = i;
		else
			inds[--n] = i;

	/* Heapify data */
	for (p = n/2; p > 0; --p)
	{
		i = p;
		t = inds[i-1];

		while (2*i+1 <= n)
		{
			j = 2*i;
			if (vals[inds[j+1-1]] > vals[inds[j-1]])
				++j;
			if (!(vals[inds[j-1]] > vals[t]))
				goto done;

			inds[i-1] = inds[j-1];
			i = j;
		}
		if (2*i <= n)
		{
			j = 2*i;
			if (!(vals[inds[j-1]] > vals[t]))
				goto done;

			inds[i-1] = inds[j-1];
			i = j;
		}

done:
		inds[i-1] = t;
	}

	/* Sort heap */
	for (p = n; p > 1; --p)
	{
		/* Pop topmost item, and move the hole down */
		i = 1;
		t = inds[i-1];

		while (2*i+1 <= p)
		{
			j = 2*i;
			if (vals[inds[j+1-1]] > vals[inds[j-1]])
				++j;

			inds[i-1] = inds[j-1];
			i = j;
		}
		if (2*i <= p)
		{
			j = 2*i;

			inds[i-1] = inds[j-1];
			i = j;
		}

		if (i < p)
		{
			/* Swap in the last item & move it up */
			inds[i-1] = inds[p-1];
			inds[p-1] = t;

			j = i;
			t = inds[j-1];

			while (j > 1)
			{
				i = j/2;
				if (!(vals[t] > vals[inds[i-1]]))
					break;

				inds[j-1] = inds[i-1];
				j = i;
			}

			inds[j-1] = t;
		}
		else
			inds[i-1] = t;
	}

	return n;
}
