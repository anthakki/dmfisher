
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "bhfdr.h"
#include "fishertest.h"
#include "fmixtest.h"
#include "rgetopt.h"
#include "sortinds.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdint.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
void *
xreallocarray(void *data, size_t count, size_t width) 
{
	size_t size;
	
	/* Compute size */
	size = count * width; /* TODO: overflow check? */

	/* Increase array size */
	data = realloc(data, size);
	if (data == NULL && size > 0)
	{
		fprintf(stderr, "error: out of memory!" "\n");
		abort();
	}

	return data;
}

struct parameter {
	/* Method for getting the parameter */
	int (*getparm)(double *, FILE *, const struct parameter *);
	/* Payload */
	union {
		int (*get)(unsigned long *, FILE *);
		double parm;
	} payload;
};

struct cookie {
	/* Method for reading an input record: B, H, L, Q, or S */ 
	int (*get)(unsigned long *, FILE *);
	/* Main function: default or adjust */
	const char *(*main)(FILE *, FILE *, const struct cookie *);
	/* Test: fisher */
	double (*test)(const unsigned long *, const double *, int);
	/* Purity, alternative purity */
	struct parameter purity1, purity2, control;
	/* Alternative hypothesis: 0, -1, or +1 */
	int tail;
	/* Adjustment function: NULL or fdr */
	void (*adjust)(double *, size_t);
	/* Output method: f, d, or s */
	int (*put)(FILE *, double);
};

#define DEFINE_GET_METHOD(_NAME, _TYPE) \
	\
	static \
	int \
	_NAME(unsigned long *record, FILE *stream) \
	{ \
		_TYPE values[2]; \
		size_t i; \
		\
		if (fread(values, sizeof(*values), countof(values), stream) != countof(values)) \
			return -1; \
		\
		for (i = 0; i < countof(values); ++i) \
			record[i] = values[i]; \
		\
		return 0; \
	}

DEFINE_GET_METHOD(m_get_B, uint8_t)
DEFINE_GET_METHOD(m_get_H, uint16_t)
DEFINE_GET_METHOD(m_get_L, uint32_t)
DEFINE_GET_METHOD(m_get_Q, uint64_t)

static
int
m_get_S(unsigned long *record, FILE *stream)
{
	/* Read records */
	if (fscanf(stream, "%lu %lu", &record[0], &record[1]) != 2)
		return -1;

	return 0;
}

static
const char *
m_main_default(FILE *dest, FILE *src, const struct cookie *cookie)
{
	unsigned long record[4];
	double parms[3], pvalue;

	/* Read loop */
	while ((*cookie->get)(&record[0], src) == 0)
	{
		/* Get the other record */
		if ((*cookie->get)(&record[2], src) != 0)
			goto read_error;

		/* Get parameters */
		if ((*cookie->purity1.getparm)(&parms[0], src, &cookie->purity1) != 0 ||
				(*cookie->purity2.getparm)(&parms[1], src, &cookie->purity2) != 0 ||
				(*cookie->control.getparm)(&parms[2], src, &cookie->control))
		{
read_error:
			if (!feof(src))
				return "read error";
			else
				return "premature end of file";
		}

		/* Perform test */
		pvalue = (*cookie->test)(record, parms, cookie->tail); /* Write */
		if ((*cookie->put)(dest, pvalue) != 0)
			return "write error";
	}

	/* Check for EOF */
	if (!feof(src))
		return "read error";

	return NULL;
}

static
const char *
m_main_adjust(FILE *dest, FILE *src, const struct cookie *cookie)
{
	double *pvalues, parms[3];
	size_t count, i;
	unsigned long record[4];

	/* Create an for the p-values */
	pvalues = NULL;
	count = 0;

	/* Read loop */
	while ((*cookie->get)(&record[0], src) == 0)
	{
		/* Read next record */
		if ((*cookie->get)(&record[2], src) != 0)
			goto read_error;

		/* Get parameters */
		if ((*cookie->purity1.getparm)(&parms[0], src, &cookie->purity1) != 0 ||
				(*cookie->purity2.getparm)(&parms[1], src, &cookie->purity2) != 0 ||
				(*cookie->control.getparm)(&parms[2], src, &cookie->control))
		{
read_error:
			/* Free data */
			free(pvalues);

			/* Return error */
			if (!feof(src))
				return "read error";
			else
				return "premature end of file";
		}

		/* Extend array */
		if ((count & (count - 1)) == 0)
			pvalues = (double *)xreallocarray(pvalues, 2*count+1, sizeof(*pvalues));

		/* Perform test */
		pvalues[count++] = (*cookie->test)(record, parms, cookie->tail);
	}

	/* Check for EOF */
	if (!feof(src))
	{
		free(pvalues);
		return "read error";
	}

	/* Adjust p-values */
	(*cookie->adjust)(pvalues, count);

	/* Write results */
	for (i = 0; i < count; ++i)
		if ((*cookie->put)(dest, pvalues[i]) != 0)
		{
			free(pvalues);
			return "write error";
		}

	/* Free data */
	free(pvalues);

	return NULL;
}
static
int
m_getparm_get(double *parm, FILE *stream, const struct parameter *param)
{
	unsigned long record[2];
	
	/* Read a pair of integers */
	if ((*param->payload.get)(record, stream) != 0)
		return -1;

	/* Convert to a fraction */
	*parm = (double)record[1] / (double)(record[0] + record[1]);

	return 0;
}

#define DEFINE_GETPARM_METHOD(_NAME, _TYPE) \
	\
	static \
	int \
	_NAME(double *parm, FILE *stream, const struct parameter *param) \
	{ \
		_TYPE value; \
		\
		if (fread(&value, sizeof(value), 1, stream) != 1) \
			return -1; \
		\
		(void)param; \
		*parm = (double)value; \
		\
		return 0; \
	}

DEFINE_GETPARM_METHOD(m_getparm_f, float)
DEFINE_GETPARM_METHOD(m_getparm_d, double)

static
int
m_getparm_parm(double *parm, FILE *stream, const struct parameter *param)
{
	/* Get constant */
	(void)stream;
	*parm = param->payload.parm;

	return 0;
}

static
double
m_test_fisher(const unsigned long *record, const double *parms, int tail)
{
	/* Run Fisher's exact test */
	(void)parms;
	return fishertest(record, tail);
}

static
double
m_test_fishermix(const unsigned long *record, const double *parms, int tail)
{
	unsigned long sumcounts;
	double *scratch, pvalue, mcomm[2], pcomm;

	/* Figure out size */
	sumcounts = record[0] + record[1] + record[2] + record[3];
	/* Allocate memory */
	scratch = (double *)xreallocarray(NULL, sumcounts+2, sizeof(scratch));

	/* Translate parameters */
	mcomm[0] = 1. - parms[0];
	mcomm[1] = 1. - parms[1];
	pcomm = parms[2];

	/* Run Fisher's exact test for mixtures */
	pvalue = fmixtest(scratch, record, mcomm, pcomm, tail);

	/* Free memory */
	free(scratch);

	return pvalue;
}

static
void
m_adjust_fdr(double *pvalues, size_t count)
{
	size_t *indices;

	/* Allocate space for sorting */
	indices = (size_t *)xreallocarray(NULL, count+1, sizeof(*indices));

	/* Sort & adjust p-values */
	bhfdr(pvalues, indices, sortinds(indices, pvalues, count));

	/* Free data */
	free(indices);
}

#define DEFINE_PUT_METHOD(_NAME, _TYPE) \
	\
	static \
	int \
	_NAME(FILE *stream, double record) \
	{ \
		_TYPE value; \
		\
		value = (_TYPE)record; \
		if (fwrite(&value, sizeof(value), 1, stream) != 1) \
			return -1; \
		\
		return 0; \
	}

DEFINE_PUT_METHOD(m_put_f, float)
DEFINE_PUT_METHOD(m_put_d, double)

static
int
m_put_s(FILE *stream, double record)
{
	/* Print record */
	if (fprintf(stream, "%.*g\n", DBL_DIG, record) < 0)
		return -1;

	return 0;
}

static
struct parameter
const_param(double parm)
{
	struct parameter param;

	/* Set it up */
	param.getparm = &m_getparm_parm;
	param.payload.parm = parm;

	return param;
}

static
void
default_cookie(struct cookie *cookie)
{
	/* Set default I/O routines */
	cookie->get = &m_get_B;
	cookie->put = &m_put_s;

	/* Default to normal Fisher, no adjustments */
	cookie->main = &m_main_default;
	cookie->test = &m_test_fisher;
	cookie->tail = 0;
	cookie->adjust = NULL;

	/* Set default parameters */
	cookie->purity1 = const_param(1.);
	cookie->purity2 = const_param(1.);
	cookie->control = const_param(-1.); /* NOTE: yields NaNs */
}

static
int
parseparamref(struct parameter *param, const char *optarg)
{
	char ch;

	/* Stop on empty argument */
	if (optarg[0] == '\0')
		return -1;

	/* Check for known short arguments */
	if (optarg[1] == '\0')
		switch (optarg[0])
		{
			/* Integer-pair readers */
			case 'B':
				{ param->getparm = &m_getparm_get;
				    param->payload.get = &m_get_B; return 0; }
			case 'H':
				{ param->getparm = &m_getparm_get;
				    param->payload.get = &m_get_H; return 0; }
			case 'I':
				/* FALLTHROUGH */
			case 'L':
				{ param->getparm = &m_getparm_get;
				    param->payload.get = &m_get_L; return 0; }
			case 'Q':
				{ param->getparm = &m_getparm_get;
				    param->payload.get = &m_get_Q; return 0; }
			case 'S':
				{ param->getparm = &m_getparm_get;
				    param->payload.get = &m_get_S; return 0; }

			/* Floating-point readers */
			case 'f':
				param->getparm = &m_getparm_f;
				return 0;
			case 'g':
				param->getparm = &m_getparm_d;
				return 0;

			default:
				break;
		}

	/* Check for a constant */
	if (sscanf(optarg, "%lg%c", &param->payload.parm, &ch) == 1)
	{
		param->getparm = &m_getparm_parm;
		return 0;
	}

	return -1;
}

static
int
parseopts(struct cookie *cookie, int argc, char *const *argv)
{
	rgetopt_t getopt;
	int result;

	/* Parse options */
	for (rgetopt_init(&getopt, argc, argv, "BHILQS p:q:c: t: a: fds");
			(result = rgetopt_next(&getopt)) != -1;)
		switch (result)
		{
			/* Binary input methods */
			case 'B': cookie->get = &m_get_B; break;
			case 'H': cookie->get = &m_get_H; break;
			case 'I': /* FALLTHROUGH */
			case 'L': cookie->get = &m_get_L; break;
			case 'Q': cookie->get = &m_get_Q; break;

			/* Text input method */
			case 'S':
				cookie->get = &m_get_S;
				break;

			/* Purity */
			case 'p':
				if (parseparamref(&cookie->purity1, getopt.optarg) != 0)
					goto invalid_optarg;

				cookie->test = &m_test_fishermix;
				break;

			/* Alternative purity */
			case 'q':
				if (parseparamref(&cookie->purity2, getopt.optarg) != 0)
					goto invalid_optarg;

				cookie->test = &m_test_fishermix;
				break;

			/* Control */
			case 'c':
				if (parseparamref(&cookie->control, getopt.optarg) != 0)
					goto invalid_optarg;

				cookie->test = &m_test_fishermix;
				break;

			/* Alternative hypothesis */
			case 't':
				switch (getopt.optarg[0])
				{
					case 'b':
						if (strcmp(getopt.optarg, "b") == 0 ||
								strcmp(getopt.optarg, "both") == 0)
						{ cookie->tail = 0; break; }
						/* FALLTHROUGH */

					case 'l':
						if (strcmp(getopt.optarg, "l") == 0 ||
								strcmp(getopt.optarg, "left") == 0)
						{ cookie->tail = -1; break; }
						/* FALLTHROUGH */

					case 'r':
						if (strcmp(getopt.optarg, "r") == 0 ||
								strcmp(getopt.optarg, "right") == 0)
						{ cookie->tail = +1; break; }
						/* FALLTHROUGH */

					default:
						goto invalid_optarg;

				}
				break;

			/* P-value adjustment */
			case 'a':
				switch (getopt.optarg[0])
				{
					case 'n':
						if (strcmp(getopt.optarg, "n") == 0 ||
								strcmp(getopt.optarg, "none") == 0)
						{ cookie->main = &m_main_default; break; }
						/* FALLTHROUGH */

					case 'f':
						if (strcmp(getopt.optarg, "f") == 0 ||
								strcmp(getopt.optarg, "fdr") == 0)
						{ cookie->main = &m_main_adjust;
						    cookie->adjust = &m_adjust_fdr; break; }
						/* FALLTHROUGH */

					default:
						goto invalid_optarg;

				}
				break;

			/* Binary output methods */
			case 'f': cookie->put = &m_put_f; break;
			case 'd': cookie->put = &m_put_d; break;

			/* Text output method */
			case 's':
				cookie->put = &m_put_s;
				break;

			/* Invalid option */
			default: /* '?' */
				fprintf(stderr, "%s: invalid option -%c" "\n",
					argv[0], getopt.optopt);
				return -1;

			/* Missing option argument */
			case ':':
				fprintf(stderr, "%s: missing argument for -%c" "\n",
					argv[0], getopt.optopt);
				return -1;

			/* Invalid option argument */
			invalid_optarg:
				fprintf(stderr, "%s: invalid argument \"%s\" for -%c" "\n",
					argv[0], getopt.optarg, getopt.optopt);
				return -1;

		}

	return getopt.optind;
}

static
void
usage(const char *argv0)
{
	/* Print usage */
	fprintf(stderr,
"Usage: %s [-BHILQS] [-pqc parm] [-t tail] [-a adjust] [-fds] file [...]" "\n"
		, argv0);
}

int
main(int argc, char **argv)
{
	struct cookie cookie;
	int optind, i;
	const char *errwhere, *errmsg;
	FILE *file;

	/* Set up defaults */
	default_cookie(&cookie);

	/* Parse arguments */
	optind = parseopts(&cookie, argc, argv);
	if (optind == -1 || !(optind < argc))
	{
		/* Print usage */
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	/* Loop over files */
	for (i = optind; i < argc; ++i)
	{
		/* File is special? */
		if (strcmp(argv[i], "-") == 0)
		{
			/* Process */
			errwhere = "(stdin)";
			errmsg = (*cookie.main)(stdout, stdin, &cookie);
		}
		else
		{
			/* Open file */
			errwhere = argv[i];
			file = fopen(errwhere, "rb");
			if (file == NULL)
			{
				errmsg = "failed to open for read";
				goto got_errmsg;
			}

			/* Process */
			errmsg = (*cookie.main)(stdout, file, &cookie);

			/* Close file */
			fclose(file);
		}

		/* Print error */
		if (errmsg != NULL)
		{
got_errmsg:
			fprintf(stderr, "%s: %s: %s\n", argv[0], errwhere, errmsg);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
