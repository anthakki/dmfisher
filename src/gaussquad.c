
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "gaussquad.h"
#include <assert.h>
#include <math.h> /* M_PI, cos() */

static
double
legendre(double *yp, size_t n, double x)
{
	double t0, t1, t2;
	size_t i;

	/*
	 * Evaluates the n:th order Legendre polynomial at x.
	 * Returns also the n-1:th order result in *yp
	 */

	assert(yp != NULL);

	/* Degenerate? */
	if (n < 1)
	{
		/* NOTE: *yp is intentionally undefined */
		return 1.;
	}

	/* Set up */
	t0 = 1.;
	t1 = x;
	/* Loop */
	for (i = 1; i++ < n;)
	{
		/* Compute next term */
		t2 = ((2*i-1)*x*t1 - (i-1)*t0) / i;
		/* Rotate */
		t0 = t1;
		t1 = t2;
	}

	*yp = t0;
	return t1;
}

void
gaussquad(double *nodes, double *weights, size_t n)
{
	size_t h, i, r;
	double pi, x, t1, t0, y;

	assert((nodes != NULL || n < 1) && (weights != NULL || n < 1));

	/* Get number of non-trivial nodes */
	h = n/2;

	/* Get pi */
#ifdef M_PI
	pi = M_PI;
#else
	pi = 4*atan(1.);
#endif

	/* Find them */
	for (i = 0; i < h; ++i)
	{
		/* Initial guess from Tricomi's asymptotic expansion */
		x = -cos((i+.75)/(n+.5) * pi);

		/* Use Newton's method to find the exact root.
		 * The number of rounds has been determined experimentally
		 */
		for (r = 0; r < 5; ++r)
		{
			/* Evaluate function */
			t1 = legendre(&t0, n, x);
			/* Update */
			y = n * (x*t1 - t0);
			x = x + t1*(1-x*x)/y;
		}

		/* Store node & weight */
		nodes[i] = x;
		weights[i] = 2*(1-x*x)/(y*y);
	}

	/* Put the trivial zero at zero for odd order */
	if (2*h < n)
	{
		/* Put zero */
		nodes[h] = 0.;
		/* Compute weight at zero */
		y = (n+1) * legendre(&t0, n+1, 0.);
		weights[h] = 2/(y*y);
	}

	/* Derive the other half of the roots */
	for (i = 0; i < h; ++i)
	{
		/* Reflect about x=0 */
		nodes[(n-1)-i] = -nodes[i];
		weights[(n-1)-i] = weights[i];
	}
}
