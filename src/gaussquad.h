#ifndef GAUSSQUAD_H_
#define GAUSSQUAD_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Computes nodes & weights for Gauss-Legendre quadrature */
void gaussquad(double *nodes, double *weights, size_t order);

#ifdef __cplusplus
}
#endif

#endif /* GAUSSQUAD_H_ */
