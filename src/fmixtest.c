
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "fmixtest.h"
#include "gaussquad.h"
#include <assert.h>
#include <math.h> /* exp(), log() */

static
void
unordered_interleave(double *x, size_t n)
{
	size_t i;
	double t;

	/*
	 * Interleaves the arrays [x,x+n) and [x+n, x+2*n)
	 * but loses the order of the output pairs
	 */ 

	if (n % 2 == 0)
		/* For even order */
		for (i = 1; i < n; i += 2 )
		{
			/* Swap misplaced elements */
			t = x[i];
			x[i] = x[n+i-1];
			x[n+i-1] = t;
		}
	else
	{
		/* For odd order */
		t = x[n-1];
		for (i = 1; i < n-1; i += 2 )
		{
			/* Rotate & swap misplaced */
			x[n+i-2] = x[i];
			x[i] = x[n+i-1];
			x[n+i-1] = x[n+i];
		}
		x[2*n-2] = t;
	}
}

static
double
lnnchoosek(unsigned long n, unsigned long k)
{
	double y;
	unsigned long j;

	/*
	 * Computes the logarithm of a binomial coefficient n choose k
	 */

	/* Factor in the values */
	y = 0.;
	for (j = 1; j++ < n-k;)
		y -= log((double)j);
	for (j = k; j++ < n;)
		y += log((double)j);

	return y;
}

static
double
lnhygepdf(unsigned long x, unsigned long m, unsigned long n, unsigned long k)
{
	/* Compute logarithm of a hypergeometric pmf */
	return lnnchoosek(m,x) + lnnchoosek(n,k-x) - lnnchoosek(m+n,k);
}

static
double
fmixadj(const double *quad, size_t quadord, double lnscale, double a1, double b1, unsigned long k1, unsigned long l1, double a2, double b2, unsigned long k2, unsigned long l2)
{
	double s, lnf;
	size_t i;

	/* 
	 * Computes the adjustment wrt regular Fisher's test.
	 * For this, lnscale := -betaln(k1+k2+1, n1-k1+n2-k2+1)
	 */

	/* Integrate.. */
	s = 0.;
	for (i = 0; i < quadord; ++i)
	{
		/* Compute logarithm of the function to be integrated */
		lnf = k1*log( b1 + a1*quad[2*i] ) + l1*log( 1.-b1 - a1*quad[2*i] ) +
			k2*log( b2 + a2*quad[2*i] ) + l2*log( 1.-b2 - a2*quad[2*i] );

		/* Accumulate */
		s += quad[2*i+1] * exp(lnscale + lnf);
	}

	return s;
}

double
fmixtest(void *scratch, const unsigned long *counts, const double *mcomm, double pcomm, int tail)
{
	static const double lnrelerr = 1e-7;

	unsigned long k1, n1, k2, n2, lo, hi, x;
	double b1, a1, b2, a2, *quad, lnscale, yp, y, ss[5], sa;
	size_t quadord, i;

	/* Get table elements */
	k1 = counts[1];
	n1 = k1 + counts[0];
	k2 = counts[3];
	n2 = k2 + counts[2];

	/* Compute bounds */
	lo = 0;
	if (k1+k2 > n2)
		lo = k1+k2 - n2;
	hi = k1+k2;
	if (k1+k2 > n1)
		hi = n1;

	/* Get coefficients of the mixing model */
	b1 = mcomm[0] * pcomm;
	a1 = 1. - mcomm[0];
	b2 = mcomm[1] * pcomm;
	a2 = 1. - mcomm[1];

	/* Set up nodes for the quadrature */
	quadord = ( n1+n2+1 + (2-1) ) / 2;
	quad = (double *)scratch;
	gaussquad(&quad[0], &quad[quadord], quadord);

	/* Interleave the array and ldjust the nodes from (-1, 1) to (0, 1) */
	unordered_interleave(quad, quadord);
	for (i = 0; i < quadord; ++i)
	{
		quad[2*i] = .5*quad[2*i] + .5;
		quad[2*i+1] = .5*quad[2*i+1];
	}

	/* Find scaling coefficient */
	lnscale = log(n1+n2+1) + lnnchoosek(n1+n2,  k1+k2);

	/* Integrate */
	{
		/* Get probability at the pivot value */
		yp = lnhygepdf(k1, n1, n2, k1+k2) + log(fmixadj(quad, quadord, lnscale,
			a1, b1, k1, n1-k1, a2, b2, k2, n2-k2));

		/* Zero sums */
		ss[0]=ss[1]=ss[2]=ss[3]=ss[4]=0.;

		/* Sum left tail */
		for (x = lo; x < k1; ++x)
		{
			y = lnhygepdf(x, n1, n2, k1+k2) + log(fmixadj(quad, quadord, lnscale,
				a1, b1, x, n1-x, a2, b2, k1+k2-x, n2-(k1+k2-x)));
			(&ss[0])[y <= yp + lnrelerr] += exp(y);
		}

		/* Pivot element */
		ss[2] = exp(yp);

		/* Sum right tail */
		for (x = k1; x++ < hi;)
		{
			y = lnhygepdf(x, n1, n2, k1+k2) + log(fmixadj(quad, quadord, lnscale,
				a1, b1, x, n1-x, a2, b2, k1+k2-x, n2-(k1+k2-x)));
			(&ss[3])[y <= yp + lnrelerr] += exp(y);
		}

		/* Sum all */
		sa = ss[0]+ss[1]+ss[2]+ss[3]+ss[4];
	}

	/* Sum correct tail */
	switch ((tail > 0) - (tail < 0))
	{
		/* NOTE: Tails flipped as k1 ~ counts[1] */

		case +1:
			return (ss[0]+ss[1]+ss[2])/sa;
		case -1:
			return (ss[2]+ss[3]+ss[4])/sa;
		default:
			return (ss[1]+ss[2]+ss[4])/sa;
	}
}
