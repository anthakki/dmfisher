
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "rgetopt.h"
#include <assert.h>
#include <stddef.h>
#include <string.h>

static
int
isvalidopt(int opt)
{
	/* TODO: extend option set, but don't allow '-', ':', '?', and friends */

	/* Validate option */
	return strchr("abcdefghijklmnopqrstuvwxyz" "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"0123456789", opt) != NULL;
}

static
void
parseoptstr(char *flags, const char *optstr)
{
	int lastopt;
	size_t i;

	assert(flags != NULL && optstr != NULL);

	/* Zero flags */
	memset(flags, '?', (size_t)CHAR_MAX + 1u);
	
	/* Parse */
	lastopt = -1;
	for (i = 0; optstr[i] != '\0'; ++i)
		if (isvalidopt(optstr[i]))
		{
			/* Flag option */
			lastopt = optstr[i];
			flags[lastopt] = '.';
		}
		else if (optstr[i] == ':' && lastopt != -1)
		{
			/* Flag for argument */
			flags[lastopt] = ':';
		}
}

static
void
rgetopt_reset(rgetopt_t *self)
{
	assert(self != NULL);

	/* Reset parser */
	self->optind = 1;
	self->optopt = '?';
	self->used_ = 1;
	self->optarg = NULL;
}

void
rgetopt_init(rgetopt_t *self, int argc, char *const *argv, const char *optstr)
{
	assert(self != NULL && argv != NULL && optstr != NULL);

	/* Parse option string */
	parseoptstr(self->flags_, optstr);

	/* Set up argument vector */
	(void)argc;
	self->argv = argv;

	/* Reset parser */
	rgetopt_reset(self);
}

static
void
rgetopt_shift_(rgetopt_t *self)
{
	assert(self != NULL);
	
	/* Point to the beginning of the next argument */
	++self->optind;
	self->used_ = 1;
}

int
rgetopt_next(rgetopt_t *self)
{
	assert(self != NULL);

	/* Check halting conditions */
	if (self->argv[self->optind] == NULL) /* out of arguments */
		return -1;
	if (self->argv[self->optind][0] != '-')  /* non-option */
		return -1;
	if (self->argv[self->optind][1] == '\0') /* "-" */
		return -1;

	/* Check for "--" */
	if (self->argv[self->optind][1] == '-' &&
			self->argv[self->optind][2] == '\0')
		/* Skip it & we're done */
	{
		rgetopt_shift_(self);
		return -1;
	}

	/* Get option */
	self->optopt = self->argv[self->optind][self->used_++];

	/* Check validity */
	switch (self->flags_[self->optopt])
	{
		case '.':
			/* No option argument, shift in next arg if out of options */
			if (self->argv[self->optind][self->used_] == '\0')
				rgetopt_shift_(self);
			break;

		case ':':
			/* Pull option argument */
			if (self->argv[self->optind][self->used_] == '\0')
			{
				/* Missing option argument */
				if (self->argv[++self->optind] == NULL)
					return ':';

				/* Get option */
				self->optarg = &self->argv[self->optind][0];
			}
			else
				self->optarg = &self->argv[self->optind][self->used_];

			/* Shift in next argument */
			rgetopt_shift_(self);

			break;

		default:
			return '?';
	}

	return self->optopt;
}
