
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "bhfdr.h"
#include <assert.h> 

static
void
bhgenfdr(double *pvals, const size_t *inds, size_t count, double scale)
{
	size_t i;
	double newp, p;

	assert((pvals != NULL || count < 1) && (inds != NULL || count < 1));

	/* Loop */
	p = 1.;
	for (i = count; i-- > 0;)
	{
		/* Compute adjusted value */
		newp = scale / (i+1) * pvals[inds[i]];
		if (newp < p)
			p = newp;

		/* Update */
		pvals[inds[i]] = p;
	}
}

void
bhfdr(double *pvals, const size_t *inds, size_t count)
{ return bhgenfdr(pvals, inds, count, (double)count); }

static
double
harmonic(size_t index)
{
	double result;

	/* Compute using the series */
	result = 0.;
	for (; index > 0; --index) /* NB. reverse loop for accuracy */
		result += 1. / index;

	return result;
}

void
byfdr(double *pvals, const size_t *inds, size_t count)
{ return bhgenfdr(pvals, inds, count, (double)count * harmonic(count)); }
