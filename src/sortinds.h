#ifndef SORTINDS_H_
#define SORTINDS_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Create sorted indices, return number of non-NaNs */
size_t sortinds(size_t *inds, const double *vals, size_t count); 

#ifdef __cplusplus
}
#endif

#endif /* SORTINDS_H_ */
