#ifndef FISHMIXTEST_H_
#define FISHMIXTEST_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Performs a test similar to Fisher's exact test, but assuming that the 
 * data are generated from mixtures with a common component. */
double fmixtest(void *scratch, const unsigned long *counts,
	const double *mcomm, double pcomm, int tail);

#ifdef __cplusplus
}
#endif

#endif /* FISHMIXTEST_H_ */
