#ifndef BHFDR_H_
#define BHFDR_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Adjust sorted p-values using the Benjamin-Hochberg procedure */ 
void bhfdr(double *pvals, const size_t *inds, size_t count);

/* Like above, but uses the Yakutieli adjustment for dependence */
void byfdr(double *pvals, const size_t *inds, size_t count);

#ifdef __cplusplus
}
#endif

#endif /* BHFDR_H_ */
