#ifndef RGETOPT_H_
#define RGETOPT_H_

/*
 * Re-entrant getopt-style option parser
 */

#include <limits.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Context for option parsing */
typedef struct rgetopt_ rgetopt_t;

/* Use self->optind, self->optopt, and self->optarg 
 * for current option index, flag, & argument
 */

/* Set up a new parser */
void rgetopt_init(rgetopt_t *self, int argc, char *const *argv, const char *optstr);

/* Get next option */
int rgetopt_next(rgetopt_t *self);

/*- Guts--don't look */

struct rgetopt_ {
	char flags_[(size_t)CHAR_MAX + 1u];
	char *const *argv;
	int optind, optopt;
	size_t used_;
	char *optarg;
};

#ifdef __cplusplus
}
#endif

#endif /* RGETOPT_H_ */
