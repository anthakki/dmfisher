
dmfisher
========

`dmfisher` is a command line interface tool for identifying differentially methylated sites using Fisher's exact test. This is a fairly standard procedure and included in most statistical packages, such as R and MATLAB, but `dmfisher` comes with options to correct the test for different sample purities (i.e. different fractions of cells of interest).

There is a command line program `dmfisher` and interfaces for the computational routines in C, R, and MATLAB. Please refer to the `dmfisher` man page (or the correspoding HMTL) or R and MATLAB documentation for detailed usage.

To build the program, you will need a C90 compiler, stdint.h (part of C99, POSIX, and most modern compilers), and a POSIX make. Issue `make` to build the tool and C libraries, and `make -C r` or `make -C matlab` to build the R and MATLAB interfaces.

All files are subject to Mozilla Public License v. 1.1. See `LICENSE.txt` for details.
Copyright (c) 2016 Antti Hakkinen.
