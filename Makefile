
# This file is subject to Mozilla Public License
# Copyright (c) 2016 Antti Hakkinen

CC = cc
AR = ar
RM = rm -f

CFLAGS = -DNDEBUG -O3 -fomit-frame-pointer -fPIC $(CFLAGS.EXTRA)
LIBS = -lm $(LIBS.EXTRA)

name = dmfisher

bin_objects = src/$(name).o src/rgetopt.o
lib_objects = src/bhfdr.o src/fishertest.o src/fmixtest.o src/gaussquad.o src/sortinds.o

objects = $(bin_objects) $(lib_objects)
targets = $(name) lib$(name).a lib$(name).so

all: $(targets)

cleanobj:
	$(RM) $(objects)

clean: cleanobj
	$(RM) $(targets)

.c.o: %.c %.h
	$(CC) $(CFLAGS) -c -o "$@" "$<"

$(name): $(bin_objects) lib$(name).a 
	$(CC) $(CFLAGS) -o "$@" $(bin_objects) lib$(name).a $(LIBS)

lib$(name).a: $(lib_objects)
	$(AR) -r -cu "$@" $(lib_objects)

lib$(name).so: $(lib_objects)
	$(CC) $(CFLAGS) -shared -o "$@" $(lib_objects) $(LIBS)

$(name).1: $(name).1.md
	pandoc --standalone --to man "$<" -o "$@"

.PHONY: all cleanobj clean
.SUFFIXES: .c.o
