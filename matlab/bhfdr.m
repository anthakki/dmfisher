
% BHFDR Adjust p-values using the Benjamini-Hochberg procedure
%   padj= BHFDR(p) adjust the p-values in p to control false discovery rate.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
