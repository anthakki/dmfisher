
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "bhfdr.h"
#include "sortinds.h"
#include <mex.h>
#include <string.h> /* memcpy() */

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isVector(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2 && (mxGetM(arg) == 1 || mxGetN(arg) == 1); }

static
int
isString(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsChar(arg); }

static
int
compareString(const mxArray *arg, const char *string)
{
	const mxChar *data;
	mwSize size, i;
	
	/* Get characters */
	data = mxGetChars(arg);
	size = mxGetNumberOfElements(arg);

	/* Compare */
	for (i = 0; i < size; ++i)
		if (string[i] == '\0' || data[i] != string[i])
			return (int)string[i] - (int)data[i];

	return (int)string[i];
}

#define MEXFILENAME "bhfdr"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	void (*method)(double *, const size_t *, size_t);
	double *pvals;
	mwSize count, *inds;

	/* Defaults */
	method = &bhfdr;

	/* Check input count */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check input */
	if (!(isFullRealDouble(prhs[0]) && isVector(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":p", "The input P must be a full real double vector.");

	/* Check optional mode argument */
	if (nrhs >= 2 && !mxIsEmpty(prhs[1]))
	{
		/* Check type */
		if (!isString(prhs[1]))
			mexErrMsgIdAndTxt(MEXFILENAME ":method", "The input METHOD must be a string.");

		/* Check value */
		switch (*mxGetChars(prhs[1]))
		{
			case 'B':
				if (compareString(prhs[1], "BH") == 0)
				{ method = &bhfdr; break; }
				if (compareString(prhs[1], "BY") == 0)
				{ method = &byfdr; break; }
				/* FALLTHROUGH */

			default:
				/* Print error */
				mexErrMsgIdAndTxt(MEXFILENAME ":method", "The input METHOD must be 'BH' or 'BY'.");
				break;

		}
	}

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(mxGetM(prhs[0]), mxGetN(prhs[0]), mxREAL);

	/* Get data */
	pvals = mxGetPr(plhs[0]);
	count = mxGetNumberOfElements(plhs[0]);

	/* Allocate scratch space */
	inds = mxMalloc(count * sizeof(*inds));

	/* Compute */
	memcpy(pvals, mxGetPr(prhs[0]), count * sizeof(*pvals));
	(*method)(pvals, inds, sortinds(inds, pvals, count));

	/* Free memory */
	mxFree(inds);
}
