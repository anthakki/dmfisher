
% FMIXTEST Fisher's exact test for knonw mixtures
%   pvalue= FMIXTEST(x, mcomm, pcomm, tail) computes the p-value of a test
%   similar to Fisher's exact test, but assuming the data are corrupted by a
%   common component.
%
% 	In particular, the setting is such that x(i,j) represents the number of
% 	observations i for the class j (x could be transposed in the definition)
% 	generated under the null hypothesis that:
%			Kj ~ Bino( sum(x(:,j)) , mcomm(j)*pcomm + (1-mcomm(j))*p(j) )
% 	where mcomm represent mixing ratios of the common component and the
% 	parametes pcomm, p(1), and p(2) specify the Bernoulli processes governing
% 	the common, first, and the second pure components. The null hypotesis is
% 	that p(1)==p(2) , and the alternative is as specified by tail. 
% 
%   The input x must be a 2-by-2 contingency table. Alternatively, x can be a
%   4-by-N matrix, in which case a test is performed for each column. Tail
%   specifies the tail of the alternative hypothesis: 'both', 'left', or
%   'right'.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
