
% FISHERTEST Fisher's exact test
%   pvalue= FISHERTEST(x, tail) computes the p-value of Fisher's exact test for
%   a 2-by-2 contingency table x. Alternatively, x can be a 4-by-N matrix, in
%   which case a test is performed for each column. Tail specifies the tail of
%   the alternative hypothesis: 'both', 'left', or 'right'.

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
