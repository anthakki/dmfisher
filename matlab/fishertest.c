
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "fishertest.h"
#include <mex.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isString(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsChar(arg); }

static
int
compareString(const mxArray *arg, const char *string)
{
	const mxChar *data;
	mwSize size, i;
	
	/* Get characters */
	data = mxGetChars(arg);
	size = mxGetNumberOfElements(arg);

	/* Compare */
	for (i = 0; i < size; ++i)
		if (string[i] == '\0' || data[i] != string[i])
			return (int)string[i] - (int)data[i];

	return (int)string[i];
}

static
int
isCount(unsigned long *count, double value)
{
	/* Cast & check */
	*count = (unsigned long)value;
	return (double)*count == value;
}

#define MEXFILENAME "fishertest"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	int tail;
	unsigned long counts[4];
	mwSize tables, j, i;
	const double *xvals;
	double *pvals;

	/* Defaults */
	tail = 0;

	/* Check input count */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check table argument */
	if (!(isFullRealDouble(prhs[0]) && ((mxGetM(prhs[0]) == 2 && mxGetN(prhs[0]) == 2) || mxGetM(prhs[0]) == countof(counts))))
		mexErrMsgIdAndTxt(MEXFILENAME ":x", "The input X must be a 2-by-2 or 4-by-N full real double matrix.");

	/* Check tail argument */
	if (nrhs >= 2 && !mxIsEmpty(prhs[1]))
	{
		/* Check type */
		if (!isString(prhs[1]))
			mexErrMsgIdAndTxt(MEXFILENAME ":tail", "The input TAIL must be a string.");

		/* Check value */
		switch (*mxGetChars(prhs[1]))
		{
			case 'b':
				if (compareString(prhs[1], "both") == 0)
				{ tail = 0; break; }
				/* FALLTHROUGH */
			case 'l':
				if (compareString(prhs[1], "left") == 0)
				{ tail = -1; break; }
				/* FALLTHROUGH */
			case 'r':
				if (compareString(prhs[1], "right") == 0)
				{ tail = +1; break; }
				/* FALLTHROUGH */

			default:
				/* Print error */
				mexErrMsgIdAndTxt(MEXFILENAME ":tail", "The input TAIL must be 'both', 'left', or 'right'.");
				break;

		}
	}

	/* Get dimension */
	tables = mxGetNumberOfElements(prhs[0]) / countof(counts);

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(tables, 1, mxREAL);

	/* Get pointers */
	xvals = mxGetPr(prhs[0]);
	pvals = mxGetPr(plhs[0]);

	/* Compute */
	for (j = 0; j < tables; ++j)
	{
		/* Check values */
		for (i = 0; i < countof(counts); ++i)
			if (!isCount(&counts[i], xvals[i + j * countof(counts)]))
				mexErrMsgIdAndTxt(MEXFILENAME ":x", "The input X must contain non-negative integers.");

		/* Perform the test */
		pvals[j] = fishertest(counts, tail);
	}
}
