
% GAUSSQUAD Gauss-Legendre quadrature
% 	[x, w]= GAUSSQUAD(n) returns the nodes x and the weights w for the n:th
% 	order Gauss-Legendre quadrature with the support (-1, 1).

% This file is subject to Mozilla Public License
% Copyright (c) 2016 Antti Hakkinen
