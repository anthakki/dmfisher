
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "gaussquad.h"
#include <mex.h>

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isCount(unsigned long *count, double value)
{
	/* Cast & check */
	*count = (unsigned long)value;
	return (double)*count == value;
}

#define MEXFILENAME "gaussquad"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	mwSize order;

	/* Check input count */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check argument */
	if (!(isFullRealDouble(prhs[0]) && mxIsScalar(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":n", "The input N must be a full real double scalar.");
	if (!isCount(&order, *mxGetPr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":n", "The input N must represent a non-negative integer.");

	/* Create outputs */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(order, 1, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(order, 1, mxREAL);

	/* Compute */
	gaussquad(mxGetPr(plhs[0]), mxGetPr(plhs[1]), order);
}
