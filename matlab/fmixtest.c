
/* This file is subject to Mozilla Public License *
 * Copyright (c) 2016 Antti Hakkinen              */

#include "fmixtest.h"
#include <mex.h>

#define countof(x) \
	(sizeof((x)) / sizeof(*(x)))

static
int
isFullRealDouble(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsDouble(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

static
int
isString(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg) && mxIsChar(arg); }

static
int
compareString(const mxArray *arg, const char *string)
{
	const mxChar *data;
	mwSize size, i;
	
	/* Get characters */
	data = mxGetChars(arg);
	size = mxGetNumberOfElements(arg);

	/* Compare */
	for (i = 0; i < size; ++i)
		if (string[i] == '\0' || data[i] != string[i])
			return (int)string[i] - (int)data[i];

	return (int)string[i];
}

static
int
isCount(unsigned long *count, double value)
{
	/* Cast & check */
	*count = (unsigned long)value;
	return (double)*count == value;
}

#define MEXFILENAME "fmixtest"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	int tail;
	unsigned long counts[4];
	mwSize tables, j, i, scratch_size, sumcounts;
	const double *xvals, *mcomm, *pcomms;
	double *pvals, *scratch;

	/* Defaults */
	tail = 0;

	/* Check input count */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments.");
	if (nrhs > 4)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments.");

	/* Check table */
	if (!(isFullRealDouble(prhs[0]) && ((mxGetM(prhs[0]) == 2 && mxGetN(prhs[0]) == 2) || mxGetM(prhs[0]) == countof(counts))))
		mexErrMsgIdAndTxt(MEXFILENAME ":x", "The input X must be a 2-by-2 or 4-by-N full real double matrix.");

	/* Get dimension */
	tables = mxGetNumberOfElements(prhs[0]) / countof(counts);

	/* Check parameters */
	if (!(isFullRealDouble(prhs[1]) && isMatrix(prhs[1]) && mxGetM(prhs[1]) == 1 && mxGetN(prhs[1]) == 2))
		mexErrMsgIdAndTxt(MEXFILENAME ":mcomm", "The input MCOMM must be a 1-by-2 full real double matrix.");
	if (!(isFullRealDouble(prhs[2]) && isMatrix(prhs[2]) && mxGetM(prhs[2]) == 1 && mxGetN(prhs[2]) == tables))
		mexErrMsgIdAndTxt(MEXFILENAME ":pcomm", "The input PCOMM must be a 1-by-%lu vector.", (unsigned long)tables);

	/* Check tail */
	if (nrhs >= 4 && !mxIsEmpty(prhs[3]))
	{
		/* Check type */
		if (!isString(prhs[3]))
			mexErrMsgIdAndTxt(MEXFILENAME ":tail", "The input TAIL must be a string.");

		/* Check value */
		switch (*mxGetChars(prhs[3]))
		{
			case 'b':
				if (compareString(prhs[3], "both") == 0)
				{ tail = 0; break; }
				/* FALLTHROUGH */
			case 'l':
				if (compareString(prhs[3], "left") == 0)
				{ tail = -1; break; }
				/* FALLTHROUGH */
			case 'r':
				if (compareString(prhs[3], "right") == 0)
				{ tail = +1; break; }
				/* FALLTHROUGH */

			default:
				/* Print error */
				mexErrMsgIdAndTxt(MEXFILENAME ":tail", "The input TAIL must be 'both', 'left', or 'right'.");
				break;

		}
	}

	/* Create output */
	(void)nlhs;
	plhs[0] = mxCreateDoubleMatrix(tables, 1, mxREAL);

	/* Get pointers */
	xvals = mxGetPr(prhs[0]);
	mcomm = mxGetPr(prhs[1]);
	pcomms = mxGetPr(prhs[2]);
	pvals = mxGetPr(plhs[0]);

	/* Initialize scratch space */
	scratch = NULL;
	scratch_size = 0;

	/* Compute */
	for (j = 0; j < tables; ++j)
	{
		/* Check values */
		for (i = 0; i < countof(counts); ++i)
			if (!isCount(&counts[i], xvals[i + j * countof(counts)]))
				mexErrMsgIdAndTxt(MEXFILENAME ":x", "The input X must contain non-negative integers.");

		/* Enlarge the temporary array */
		sumcounts = counts[0] + counts[1] + counts[2] + counts[3];
		if (scratch_size < sumcounts + 2)
		{
			scratch = mxRealloc(scratch, (sumcounts + 2) * sizeof(*scratch));
			scratch_size = sumcounts + 2;
		}

		/* Perform the test */
		pvals[j] = fmixtest(scratch, counts, mcomm, pcomms[j], tail);
	}

	/* Free data */
	mxFree(scratch);
}
